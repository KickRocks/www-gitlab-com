---
layout: markdown_page
title: "Contributor Growth"
description: "Implement key business iterations that results in substantial and sustained increases to community contributors & contributions"
canonical_path: "/company/team/structure/working-groups/contributor-growth/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 28rd, 2021 |
| Target End Date | October 28rd, 2022 |
| Slack           | [#wg_contributor-growth](https://gitlab.slack.com/archives/C0223D98HHC) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1AOgqaslnq-WI1ICSZ1NzSnALf1Va4D5qAD191icAoSI/edit#) (only accessible from within the company) |
| Task Board      | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/3014703?label_name%5B%5D=Contribution%20Efficiency) (items above the cut-line) |

## Business Goal

Implement key business iterations that results in substantial and sustained increases to community contributors & contributions

### Exit Criteria (30% completed)

1. Implement 3 experiments aimed at increasing community contributors
   1. Decreasing community MR Review time => Reduced Open Community MR age and review time in FY23Q1 
   1. Create additional retention recognition => Experimented with rewarding 
   1. Addition Outreach effort => Experimented with frontend themed months
1. Delivery key joint strategy of increasing community contributors => Strategy is complete
1. Setup first iteration of community cohort team 
1. Deliver 3 key iterations to our contribution guidelines 
1. Fully implement MR Coach specialties (Development, Test, Docs & etc)
1. Experiment with defining contributor specialties based on MR coaches 
1. Define follow up working process between Community Relations and Contributor Success teams


### Roles and Responsibilities

| Working Group Role    | Person                                               | Title                                                      |
|-----------------------|------------------------------------------------------|------------------------------------------------------------|
| Executive Sponsor     | [Mek Stittri](https://gitlab.com/meks)               | VP of Quality                                              |
| Functional Lead       | [Nick Veenhof](https://gitlab.com/kwiebers)          | Director of Contributor Success                            |
| Functional Lead       | [John Coghlan](https://gitlab.com/johncoghlan)       | Manager, Community Relations                               |
| Functional Lead       | [Christos Bacharakis](https://gitlab.com/cbacharakis)| Senior Code Contributor Program Manager                    |
| Member                | [Rémy Coutable](https://gitlab.com/rymai)            | Staff Backend Engineer, Engineering Productivity           |
| Member                | [Tanya Pazitny](https://gitlab.com/tpazitny)         | Director of Quality Engineering                            |
| Member                | [Marshall Cottrell](https://gitlab.com/marshall007)  | Principal, Strategy and Operations (Technical)             |           
